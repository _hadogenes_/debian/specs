Source: cantata
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Stuart Prescott <stuart@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libavahi-client-dev,
 libavahi-common-dev,
 libavcodec-dev,
 libavformat-dev,
 libavutil-dev,
 libcddb2-dev,
 libcdio-paranoia-dev,
 libdbus-1-dev,
 libebur128-dev,
 libmpg123-dev,
 libmtp-dev,
 libmusicbrainz5-dev,
 libqt6svg6-dev,
 libspeex-dev,
 libspeexdsp-dev,
 libtag1-dev,
 libudev-dev [linux-any],
 pkg-kde-tools,
 pkgconf,
 qt6-base-dev,
 qt6-multimedia-dev,
 qt6-tools-dev,
 qt6-tools-dev-tools,
Standards-Version: 4.7.0
Homepage: https://github.com/CDrummond/cantata
Vcs-Browser: https://salsa.debian.org/multimedia-team/cantata
Vcs-Git: https://salsa.debian.org/multimedia-team/cantata.git
Rules-Requires-Root: no

Package: cantata
Architecture: any
Depends:
 fonts-font-awesome,
 libqt6sql6-sqlite,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 libio-socket-ip-perl,
 liburi-perl,
Provides:
 mpd-client,
Suggests:
 media-player-info,
 mpd,
Description: Qt client for the music player daemon (MPD)
 Cantata is a graphical front-end for the music player daemon, MPD. It provides
 many tools to view and manage the music collection including:
 .
   * library - sorted by album, artist, track, directory
   * albums with cover art
   * user defined playlists
   * dynamic playlists
   * streams, internet radio
   * lyrics
   * artist information from Wikipedia
   * device support for USB mass storage, MTP and CDs
